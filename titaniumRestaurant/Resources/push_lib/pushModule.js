/**
 * 
 * @param {Object} p
 * @config {String} URBAN_AIRSHIP_APP_KEY
 * @config {String} URBAN_AIRSHIP_MASTER_SECRET
 */
exports.registerDisplayUrbanAirshipPush=function(p)
{
     
    Titanium.Network.registerForPushNotifications({
        types:[
            Titanium.Network.NOTIFICATION_TYPE_BADGE,
            Titanium.Network.NOTIFICATION_TYPE_ALERT,
            Titanium.Network.NOTIFICATION_TYPE_SOUND
        ],
        success: successCallback,
        error: errorCallback,
        callback: messageCallback
    });
    
    function successCallback(e) {
        var request = Titanium.Network.createHTTPClient({
            onload:function(e) {
                if (request.status != 200 && request.status != 201) {
                    request.onerror(e);
                    return;
                }
            },
            onerror:function(e) {
                Ti.API.info("Register with Urban Airship Push Service failed. Error: " + e.error);
            }
        });
     
        // Register device token with UA
        request.open('PUT', 'https://go.urbanairship.com/api/device_tokens/'+ e.deviceToken, true);
        request.setRequestHeader('Authorization','Basic '  +Titanium.Utils.base64encode(p.URBAN_AIRSHIP_APP_KEY + ':' + p.URBAN_AIRSHIP_MASTER_SECRET));
        request.send();
    }
    
    function errorCallback(e) {
        Ti.API.info("Error during registration: " + e.error);
    }
    
    function messageCallback(thePush) {
         var message;
         if(thePush['aps'] != undefined) {
              if(thePush['aps']['alert'] != undefined){
                   if(thePush['aps']['alert']['body'] != undefined){
                        message = thePush['aps']['alert']['body'];
                   } else {
                        message = thePush['aps']['alert'];
                   }
              } 
         } 
    }    
};
