var VARS=require('common/globals');
var Admob = require('ti.admob');
exports.requestAd=function(p) {
    
	var publisherID=VARS.iphonePublisher_id;
    if(VARS._platform==VARS._iPad){
		publisherID=VARS.ipadPublisher_id;
	}
	else if(VARS._platform==VARS._android){
		publisherID=VARS.androidPublisher_id;
	}
	var adMobView = Admob.createView({
   			 publisherId:publisherID,
   			 width:VARS.AdWidth,
   			 bottom:0,
   			 testing: VARS.adTesting,
   			 height:VARS.AdHeightDisplayed,  
 	});
	p.win.add(adMobView);
};
