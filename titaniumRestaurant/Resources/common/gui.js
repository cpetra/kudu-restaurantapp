/*
* @author CRISTIAN PETRA	
* @email cristy86_upm@yahoo.com
* @content GUI Module
* @version 1.0
*/
var smartButton=function(p)
{
	var theButtonBg=Ti.UI.createView(p.conf);
	theButtonBg.backgroundGradient={
		type:'linear',
		colors:[{color:VARS.buttonTopColor,position:0.0},{color:VARS.buttonBottomColor,position:1.0}]
	};
	if(p.bgGard!=null)
	{
		theButtonBg.backgroundGradient=p.bgGard;
	}
	
	theButtonBg.borderRadius=3;
	
	var theLblInside = Ti.UI.createLabel({
		textAlign : 'center',
		text : p.text,
		color : VARS.textColor,
		width : p.conf.width,
		font:{fontSize:"13dp"},
		touchEnabled:false,
	}); 
	theButtonBg.add(theLblInside);
	
	if(p.displayIndicator&&VARS._iOS)
	{
		var actIndLoading=Ti.UI.createActivityIndicator({
			left:"10dp",
			//style:Titanium.UI.iPhone.ActivityIndicatorStyle.DARK,
			width:"30dp",
			height:"30dp",
		});
		theButtonBg.add(actIndLoading);
		actIndLoading.show();
	}
	
	return theButtonBg;
	
};

/**
 * Calculate new color
 * @param {Object} param
 * - {String} baseColor, hexadecimal reprezentation
 * - {Number} r, integer value to be changed  red
 * - {Number} g, integer value to be changed  green
 * - {Number} b, integer value to be changed  blue
 * @return {String} color, hexadecimal reprezentation 
 */
var calculateColor=function(param){
    //First seprate the base color
    var base=param.baseColor.replace("#","");
    
    if(base.length!=6)
    {
        alert("Error in the color");
        return; 
    }else{
        
        var baseR=parseInt(base.substring(0,2),16)+param.r;
        if(baseR>254){baseR=255;}
        var baseG=parseInt(base.substring(2,4),16)+param.g;
         if(baseG>254){baseG=255;}
        var baseB=parseInt(base.substring(4,6),16)+param.b;
         if(baseB>254){baseB=255;}
        return("#"+baseR.toString(16)+""+baseG.toString(16)+""+baseB.toString(16));
    }
};


/**
 * Creates Smart view
 * @param {Object} p
 * - {Object} p
 * - {String} text, button text
 * - {Object} config, speccific configuration 
 * - {Object} imageConf, configuration for image
 */
var smartView=function(p)
{
    
     //var baseColor="#38aac7";
    var bottomTop=calculateColor({
        baseColor:VARS.barColor,
        r:30,
        g:33,
        b:33,
    });
    
    var theButtonBg=Ti.UI.createView(p.conf);
    theButtonBg.borderRadius=3;
    theButtonBg.borderColor="#50000000";
    theButtonBg.borderWidth="1dp";
    theButtonBg.backgroundGradient={
            type:'linear',
            colors:[{color:bottomTop,position:0.0},{color:bottomTop,position:0.5},{color:VARS.barColor,position:1.0}],
    };
   
    var topElement=Ti.UI.createView({
        width:"100%",
        height:"50%",
        top:"0dp",
        touchEnabled:false,
        backgroundGradient:{
            type:'linear',
            colors:[{color:"#20FFFFFF",position:0.0},{color:"#40FFFFFF",position:1.0}],
        },
    });
    theButtonBg.add(topElement); 
   
    return theButtonBg;
    
};
exports.smartView=smartView;


var createLoadMoreButton=function(p)
{
	var bgColor=VARS.rowColor;
	if(p.size%2==1){bgColor=VARS.rowOddColor;}
	var row = Ti.UI.createTableViewRow({
		height:"60dp",
		backgroundColor:bgColor,
		});
	row.rowtype = 'load_more_button';
	
	row.add(smartButton({
		text:L('load_more'),
		conf:{
			width:"100dp",
			touchEnabled:false,
			height:"40dp",
			},
	}));
	
	
	row.addEventListener('click', function(e) {
		e.source.add(smartButton({
			text : L('updating'),
			displayIndicator:true,
			conf : {
				width : "200dp",
				touchEnabled : false,
				height : "40dp",
			},
		}));
	});

	return row;
};

/**
 * @method createInputField
 * Creates input text form
 * 
   *     @example
   *     var useremail = GUI.createInputField({
   *             hint : L('search'),
   *             left:"10dp",
   *             width:(VARS._dpiWidth-120),
   *             keyboardType : Titanium.UI.KEYBOARD_EMAIL,
   *      })
   *      topBar.add(useremail);
   * 
 *
 * <p>Acces text value by <b>useremail.textField.value</b></p>
 * 
 * @param {Object} p
 * - {Number} width, optional
 * - {Number} top
 * - {String} hint
 * - {Boolean} passwordMask, optional
 * - {STATIC} keyboardType, optional
 * - {String} left, optional
 * - {String / Blob} thumbnail thumbnail used for this row
 */
var createInputField=function(p)
{
    var defaultWidth=VARS._dpiWidth-40;
    if(VARS._platform==VARS._iPad){defaultWidth=300;}
    var width=p.width||defaultWidth;  //Take paramter or calculate default width
    var inputFieldHolder=Ti.UI.createView({
        borderWidth:"1dp",
        borderColor:"#AAb4b4b4",
        borderRadius:"1",
        backgroundColor:"white",
        top:p.top,
        width:width+"dp",
        height:"40dp",
        left:p.left
    });
    
    var left=null;
    if(p.thumbnail){
        left="40dp";
        width-=40;
        
        var thumbnailImage=Ti.UI.createImageView({
            image:p.thumbnail,
            left:"2dp",
            height:"22dp",
        });
        inputFieldHolder.add(thumbnailImage);
    }
    var inputText=Ti.UI.createTextField({
        color:"black",
        hintText:p.hint,
        width:(width-10)+"dp",
        font:VARS.normal,
        backgroundColor:"#00ffffff",
        backgroundFocusedColor:"#00ffffff",
        borderColor:"#00ffffff",
        keyboardType:p.keyboardType,
        passwordMask:p.passwordMask,
        height:"40dp",
        left:left,
    });
    inputFieldHolder.add(inputText);
    inputFieldHolder.textField=inputText;
    
    return inputFieldHolder;
};



//####################### INTERFACE #################
exports.createInputField=createInputField;
exports.smartButton=smartButton;
exports.createLoadMoreButton=createLoadMoreButton;