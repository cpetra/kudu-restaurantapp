/**
 * 
 * 
 */

/**
 * @property Cloud
 * @type Titanium.Cloud
 */
var Cloud = require('ti.cloud');
Cloud.debug = true;
Ti.App.Properties.setBool("userLoggedIn",false);

var menuAPI = require('menu_lib/menuAPI');

//################### VIEWS & WINDOW ################
//TODO Add logo
//Create loading window

var loadingWindow=Ti.UI.createWindow({
        title:L('appname'),
       // backgroundImage:Ti.Platform.osname=="ipad"? "/images/ipad/bg.png" : "/images/bg.png",
        backgroundColor: "#5a1b1a",
        navBarHidden : false,
        extendEdges : [],
        orientationModes : [Titanium.UI.PORTRAIT],
        translucent : false,
   });
    
    var logoImage = Ti.UI.createImageView({
        top:"0dp",
        width: Ti.Platform.displayCaps.platformWidth,
     //   height:  187,
        image:"/images/logo.png",
    });
    if(Ti.Platform.osname!= 'android') {
    	if(Ti.Platform.osname=="ipad")
    		logoImage.image = '/images/logoipad.png';
    }
    	
  
    loadingWindow.add(logoImage);
	var actIndLoading=Ti.UI.createActivityIndicator({
        bottom:"40dp",
        width:Ti.UI.SIZE,
        height:Ti.UI.SIZE,
   });
    
//##################### LOAD SETTINGS ############
//Function to load settings from ACS
loadSettings=function(){
	
	menuAPI.createTableOnApplicationStarted();
	Titanium.API.info('LANGUAGE:----' + Ti.Locale.currentLanguage);
	Titanium.API.info('-----app started');
	Titanium.App.fireEvent('applicationStarted');
	Titanium.App.fireEvent('applicationHomeLOADED');
    
	//check if it's loading first time and not internet connection available
	if( Ti.Network.NETWORK_NONE===Ti.Network.networkType && Ti.App.Properties.getObject('settings')==null ){
		actIndLoading.hide();
		Ti.include('appgen.js');
		return;
	}
	
	//read the settings only on the first run, on this way will work on offline mode
	if( Ti.App.Properties.getObject("settings") ) {
		Titanium.API.info('settings is already set');
		actIndLoading.hide();
		Ti.include('appgen.js');
	}
	else {
	    Cloud.Objects.query({
	        classname : 'settings',
	        page : 1,
	        per_page : 100,
	    }, function(e) {
	        if (e.success) {
	         //   Ti.API.info(e.settings);
	            actIndLoading.hide();
	            Ti.App.Properties.setObject("settings",e.settings[0]);
	            Ti.include('appgen.js');
	        } else {
	            //TODO Alert that internet connection is required
	        }
	    });
    }
};

//Events
Titanium.App.addEventListener('changeLanguageEvent', function(e) {
	
	//loadingWindow.close();
});

//SET UP
loadingWindow.add(actIndLoading);
actIndLoading.show();
loadingWindow.open();
loadSettings();


