/*
* @author DANIEL DIMOV
* @email dimovdaniel@yahoo.com
* @content maps Module
* @version 2.0
*/

//Include Required modules
var VARS =require('common/globals');
var GUI =require('maps_lib/mapGUI');    
var mapAPI = require('maps_lib/mapAPI');

var MapModule = require('ti.map');


var mapview = [];
var currentLatitude;
var currentLongitude;
	//Array for the locations
var locations = [];
var mapCreated = false;
var currentRegion = null;
var annotationAdded = false;
var locationCreated = false;
var win = [];
/**
 * @description Creates window with map
 * @param {Object} p, Parameters
 * @config {Window} win, the window to open the gallery in
 * @config {String} title, the window title
 * @config {Int} type, type of the distance [0-In kilometers , 1 - in Milles ]
 * @config {Int} mapType, type of the map [0-In kilometers , 1 - in Milles ]
 * @config {String} tab, tab to open sub windows
 * @config {String} locations, Location Object
 */
exports.createMap = function(p) 
{
	//alert('create map');
//################ DATA ###################
    var pointAdded=false;
	var MapModule = require('ti.map');

	if(Titanium.Platform.name == VARS._android) {
		var rc = MapModule.isGooglePlayServicesAvailable();
		switch (rc) {
		    case MapModule.SUCCESS:
		        //alert('Google Play services is installed.');
		        break;
		    case MapModule.SERVICE_MISSING:
		        alert('Google Play services is missing. Please install Google Play services from the Google Play store.');
		        break;
		    case MapModule.SERVICE_VERSION_UPDATE_REQUIRED:
		        alert('Google Play services is out of date. Please update Google Play services.');
		        break;
		    case MapModule.SERVICE_DISABLED:
		        alert('Google Play services is disabled. Please enable Google Play services.');
		        break;
		    case MapModule.SERVICE_INVALID:
		        alert('Google Play services cannot be authenticated. Reinstall Google Play services.');
		        break;
		    default:
		        alert('Unknown error.');
		        break;  
		};
	}

	createLocations(p.locations);

	//############### VIEWS #####################
	createMapView();

	//#################### FUNCTIONS ###################


	//#################### EVENT LISTENRS ##############
   /*
    mapview.addEventListener('click', function(e) {
        if (e.clicksource == 'infoWindow' || e.clicksource == 'subtitle' ||e.clicksource == 'title' || e.clicksource == 'rightButton' || e.clicksource == 'leftPane' || e.clicksource == 'rightView') {
            if (VARS._platform == VARS._android) {
            	
                var intent = Ti.Android.createIntent({
                    action : Ti.Android.ACTION_VIEW,
                    data : 'geo:'+e.annotation.latitude+','+e.annotation.longitude
                });
                Ti.Android.currentActivity.startActivity(intent);
                
                //Ti.Platform.openURL("http://maps.google.com/maps?q=" + e.annotation.latitude + "," + e.annotation.longitude);
            } else {
              //  Ti.Platform.openURL("http://maps.google.com/maps?q=" + e.annotation.latitude + "," + e.annotation.longitude);
            }

        };
    });
	*/
	
	//win = p.win;
	p.win.add(mapview);
};

function createLocations(p) {
	
	//if(locationCreated)
	//	return;
	Ti.API.info('create locations');
	locations = [];	
	for( i = 0; i < p.length; i++) 
	{	
		var location = p[i].store;
		Ti.API.info(location.latlon.split(',')[0] + '    ' + location.latlon.split(',')[1]);
		var titleTxt = ( Ti.Locale.currentLanguage=='ar'&& location.arabic_title.length > 0 ) ? location.arabic_title : location.title;
		var l = MapModule.createAnnotation({
			latitude : location.latlon.split(',')[0],
			longitude : location.latlon.split(',')[1],
			title : titleTxt,
			//subtitle : location.subTitle ? location.subTitle : location.options + '   ' +location.phone ,
			subtitle: L('distance')  + ': ' + location.distance_proximity,
			//image: "/images/markerfill_new.png",
			//image: '/images/place.png',
			pincolor : MapModule.ANNOTATION_RED,
			animate : true,
			rightButton : Titanium.UI.iPhone.SystemButton.INFO_LIGHT,
			rightView:"/images/place.png",
			myid : i // CUSTOM ATTRIBUTE THAT IS PASSED INTO EVENT OBJECTS
		});
		
		locations.push(l);
	}	
	locationCreated = true;
}


function currentLocation(){
	Titanium.Geolocation.getCurrentPosition(function(e){
		if (e.error) {
        	return; 
    	}
    	
		
        currentRegion = {
            latitude: e.coords.latitude,
            longitude: e.coords.longitude,
            animate:true,
            latitudeDelta:0.1,
            longitudeDelta:0.1
        };
        currentLatitude = e.coords.latitude;
        currentLongitude = e.coords.longitude;
        
        Titanium.API.info('current pos:' + currentLatitude + '    ' + currentLongitude);
	    
        createMapView();        
        mapview.setLocation(currentRegion);
    	mapSetCurrentLocation();
    	
    	var info = {};
		info.currentLatitude = currentLatitude;
		info.currentLongitude = currentLongitude;
		
		readAnnotations({info:info}); 
   		mapview.addEventListener('postlayout', mapViewAddAnnotation);
        Titanium.App.Properties.setBool('currentPossitionSet', true);
      });// END GET CURRENT POSITION FUNCTION 

      if( Titanium.App.Properties.getBool('currentPossitionSet') );
      	Titanium.Geolocation.removeEventListener('location', currentLocationHandler);
}

Titanium.Geolocation.purpose = "Receive User Location";
Titanium.Geolocation.frequency=1;
Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_HIGH;
Titanium.Geolocation.locationServicesEnabled = true; 
Titanium.Geolocation.addEventListener('location', currentLocationHandler);	
 
removeCurentLocationHandler = function() {
	
}
exports.removeCurentLocationHandler = removeCurentLocationHandler;

function currentLocationHandler() {
	 currentLocation(); 
}

mapViewAddAnnotation = function(e){
	
	//mapview.addAnnotations(locations);
	addAnnotation(locations);
};

addAnnotation = function(param){
	//if(mapview!= null && annotationAdded!= false) {
		mapview.addAnnotations(param);
	//	annotationAdded = true;
	//}	
}


function createMapView() {
	
	if(mapCreated)
		return;

	checkInternetConnection();
	
	var zoomLevelAndroid= parseInt("8");
    var zoomLevelIos=parseFloat("8");
	var theRegion= {
        //    latitude : p.locations[0].latlon.split(',')[0],
        //    longitude : p.locations[0].latlon.split(',')[1],
        }
        if(VARS._iOS){
            theRegion.latitudeDelta=zoomLevelIos;
            theRegion.longitudeDelta=zoomLevelIos;
        }else{
           theRegion.zoom=zoomLevelAndroid;
        }
	 
	mapview = MapModule.createView({
		animate : true,
		regionFit : true,
		userLocation : true
	});
	
	mapCreated = true;
	Titanium.API.info('end create map view');
	mapSetCurrentLocation();
}

function mapSetCurrentLocation() {
	if(currentRegion!= null && mapview != null && currentRegion!=null ) {
		Titanium.API.info('map set current location:' + currentRegion.latitude);
		mapview.setLocation(currentRegion);
		mapview.setRegion(currentRegion);
	}
}

function checkInternetConnection() {
	
	if (Titanium.Network.networkType === Titanium.Network.NETWORK_NONE) {
		
		var alertDialog = Titanium.UI.createAlertDialog({
			title: 'WARNING!',
			message: 'Your device is not online.',
			buttonNames: ['OK']
		});
		//alertDialog.show();
		return false;
	} else {
	   return true;
	}
	
}

Titanium.App.addEventListener('annotationReaded', function(e) {
	//alert('annotation readed');
	//addAnnotation(VARS._MapLocations)
	createLocations(VARS._MapLocations);
	addAnnotation(locations);
})
