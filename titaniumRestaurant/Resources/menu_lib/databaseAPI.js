populateTableMenuCategoryFirstTime = function(p) {
	var categories = {};
	categories = p.categories;	
	menucategory_db = Titanium.Database.open('menucategory_db');
	menucategory_db.execute('CREATE TABLE IF NOT EXISTS menucategory_tb(id INTEGER PRIMARY KEY,  title TEXT, image TEXT, thumb TEXT, updated DATE, weight INT, arabic_title TEXT )');

	for(var i=0; i<categories.length; i++){
		var category = categories[i].category;
		Titanium.API.info(category);
		menucategory_db.execute('INSERT INTO menucategory_tb (id, title, image, thumb, updated, weight, arabic_title) VALUES (?,?,?,?,?,?,?)', category.id,  category.title, category.image, category.thumb, category.updated, category.weight, category.arabic_title);	
	}
	menucategory_db.close();
};

populateTableMenuItemsFirstTime = function(p) {
	Titanium.API.info('populate table menu items first time');
	var menus = {};
	menus = p.menus;	
	menucategory_db = Titanium.Database.open('menucategory_db');
	menucategory_db.execute('CREATE TABLE IF NOT EXISTS menuitems_tb(id INTEGER PRIMARY KEY, cid INTEGER, title TEXT, image TEXT, category TEXT,  updated DATE, price NUMBER, thumb TEXT, weight INT, arabic_title TEXT, arabic_url TEXT, english_url TEXT, description TEXT, arabic_description TEXT   )');
	for(var i=0; i<menus.length; i++){
		var menu = menus[i].menu;
		Titanium.API.info(menu);
		menucategory_db.execute('INSERT INTO menuitems_tb(id, cid, title, image, category, updated, price, thumb, weight, arabic_title, arabic_url, english_url, description, arabic_description) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)', menu.id, menu.cid, menu.title, menu.image, menu.category, menu.updated, menu.price, menu.thumb, menu.weight, menu.arabic_title, menu.arabic_url, menu.english_url, menu.description, menu.arabic_description);	
	}
	menucategory_db.close();		
};

applyChangesMenuItems = function(content) {
	Titanium.API.info("apply changes menus");
	db = Titanium.Database.open('menucategory_db');
	var sql =
            'INSERT OR REPLACE INTO menuitems_tb (id, cid, title, image, category,  updated, price, thumb, weight, arabic_title, arabic_url, english_url, description, arabic_description) ' +
            'VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
	var categories = content;
	Titanium.API.info('length :' + categories.length);
	for(var i=0; i<categories.length; i++){
		var category = categories[i];
		//Titanium.API.info(category);
		var params = [category.id, category.cid, category.title, category.image, category.category, category.updated, category.price, category.thumb, category.weight, category.arabic_title, category.arabic_url, category.english_url, category.description, category.arabic_description ];
		db.execute(sql, params); 
	}
	db.close();
};

applyChangesCategories = function(content) {
	Titanium.API.info("apply changes categories");
	db = Titanium.Database.open('menucategory_db');
	var sql =
            'INSERT OR REPLACE INTO menucategory_tb (id, title, image,  updated, thumb, weight, arabic_title) ' +
            'VALUES (?, ?, ?, ?, ?, ?, ?)';
	var categories = content;
	Titanium.API.info('length :' + categories.length);
	for(var i=0; i<categories.length; i++){
		category = categories[i];
		var params = [category.id, category.title, category.image, category.updated, category.thumb, category.weight, category.arabic_title ];
		db.execute(sql, params); 
	}
	db.close();	
};

exports.populateTableMenuCategoryFirstTime = populateTableMenuCategoryFirstTime;
exports.populateTableMenuItemsFirstTime = populateTableMenuItemsFirstTime;
exports.applyChangesMenuItems = applyChangesMenuItems;
exports.applyChangesCategories = applyChangesCategories;
