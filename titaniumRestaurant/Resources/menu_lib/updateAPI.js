var databaseAPI = require('menu_lib/databaseAPI');

getChanges = function(syncURL, modifiedSince, callback) {
	var sendit = Ti.Network.createHTTPClient({ 
         onerror: function(e){ 
               Ti.API.info(e.error);
               //alert('There was an error during the connection item'); 
         }, 
      	timeout:10000, 
	});
	var url = syncURL + '?changed=' + modifiedSince ;
	Titanium.API.info('url---' + url);
	sendit.open('GET', url);  
	sendit.send(); 
	//Function to be called upon a successful response 
	sendit.onload = function(){ 
		var jsonUpdate = JSON.parse(this.responseText) ;
		Titanium.API.info('changes');
		Titanium.API.info( this.responseText );
		callback({changes: JSON.parse(this.responseText) });
	 };   
};

getLastSyncItems = function() {
	var db = Titanium.Database.open('menucategory_db');
	var rows = db.execute('SELECT MAX(updated) as lastSync FROM menuitems_tb');	
	var lastSync;
	while(rows.isValidRow()){ 
		Titanium.API.info('::::: +' + rows.fieldByName('lastSync'));
		lastSync = rows.fieldByName('lastSync');
		rows.next();
	}
	rows.close();
	db.close();
	Titanium.API.info('last sync menu items' + lastSync);
	return lastSync;
};

applyChanges = function(changes) {
	var contents = changes;
	var menuItems = [];
	var categoryItems = [];
	Titanium.API.info('changes');
	//Titanium.API.info(changes);	
	for (var i=0; i< contents.length; i++) {
		var content = contents[i].content;

		if(content.type == "menu") {
			menuItems.push(content);
		}
		else if(content.type == "category") {
			categoryItems.push(content);
		}
	}
	
	applyChangesMenuItems(menuItems);
	applyChangesCategories(categoryItems);
};

exports.getLastSyncItems = getLastSyncItems;
exports.getChanges = getChanges;
exports.applyChanges = applyChanges;
