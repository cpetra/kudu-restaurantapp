var VARS =require('common/globals');
var Window = require('ui/handheld/ApplicationWindow');

/**
 * @method createCategoryRow
 * Creates category menu images
 * @param {Object} p
 * - {Object} category, category to create row for
 * @return {Ti.UI.VIEW} row Row to be displayed
 */

function createCategoryRow(p){
    //############### DATA ###############
    //TODO ADD CODE
    
  
    //############### VIEWS ###############
    var row=Ti.UI.createTableViewRow({
        height:VARS._platform==VARS._iPad?"324dp":"145dp",
        width:"100%",
        id:p.category.id,
        categoryTitle: Ti.Locale.currentLanguage=='ar' ? p.category.arabic_title : p.category.title
    });
    
    if(p.category.photo){
        var image=Ti.UI.createImageView({
            image:p.category.photo,
            width:"100%",
            height:VARS._platform==VARS._iPad?"324dp":"135dp",
            top: "-10dp",
            touchEnabled:false,
            preventDefaultImage:true,
        });
        row.add(image);
    }
    
    var titleHolder=Ti.UI.createView({
       // width:"auto",
        height:"30dp",
        //backgroundColor:"#77000000",
        backgroundColor:"#9e1e20",
        bottom:"0dp",
        left:"0dp",
        touchEnabled:false,
        //opacity:0.8,
    });
    row.add(titleHolder);
    
    var title=Ti.UI.createLabel({
        bottom:"0dp",
        height:"30dp",
        left:"10dp",
        right:"10dp",
        text: Ti.Locale.currentLanguage =='ar' ? "    "+p.category.arabic_title+"   "    :  "    "+p.category.title+"   ",
        color:VARS.textSecondColor,
        font : {
            fontFamily : VARS._iOS ? 'Raleway' : 'Raleway-Regular',
            fontSize : "18dp",
            fontStyle:"bold",
            fontWeight:"bold",
         },
         textAlign: Ti.Locale.currentLanguage =='ar' ? Ti.UI.TEXT_ALIGNMENT_RIGHT : Ti.UI.TEXT_ALIGNMENT_LEFT,
         touchEnabled: false
    });
    row.add(title);
    
  
    //############### FUNCTIONS ###############
    //TODO ADD CODE
  
    //############### EVENT LISTENERS ###############
    //TODO ADD CODE
  
    //############### SETUP AND RETURN ###############
    //TODO ADD CODE
    return row;
}
exports.createCategoryRow=createCategoryRow;

/**
 * @method createMenuRow
 * Creates specific item row
 * @param {Object} p
 * - {Object} item, the item to be displayed in
 * @return {Ti,UI.TableViewRow} row The row to be inserted into the list
 */
function createMenuRow(p){
    //############### DATA ###############
    //TODO ADD CODE

    //############### VIEWS ###############
     var row=Ti.UI.createTableViewRow({
         width:"100%",
         height:"120dp",
         index:p.item.index,
         backgroundColor:"#00FFFFFF",
     });
     
     var dataHolder=Ti.UI.createView({
         width:p.item.photo ? (VARS._dpiWidth-170)+"dp" : (VARS._dpiWidth-20)+"dp",
         touchEnabled:false,
         height:"110dp",
         borderRadius:"8dp",
         backgroundColor:VARS.rowColor,
         index:p.item.index,
         left:p.item.photo ? "160dp":null,
     });
     
     if(Ti.Platform.osname != 'android') {
   		if(VARS._platform!=VARS._iPad) {
   			dataHolder.width =  p.item.photo ? (VARS.platformWidth - 170)+'dp' : (VARS.platformWidth - 20)+'dp';
   		}
   	}
     
     
     var title=Ti.UI.createLabel({
         text: Ti.Locale.currentLanguage=='ar' ? p.item.arabic_title  : p.item.title,
        // width:dataHolder.width,
         font : {
            fontFamily : VARS._iOS ? 'Signika' : 'Signika-Light',
            fontSize : "13dp",
         },
         top:"0dp",
         left:"5dp",
         right:"5dp",
         height:"20dp",
         color:VARS.textColor,
         touchEnabled:false,
         index:p.item.index,
         textAlign: Ti.Locale.currentLanguage=='ar' ? Ti.UI.TEXT_ALIGNMENT_RIGHT : Ti.UI.TEXT_ALIGNMENT_LEFT
     });
     dataHolder.add(title);
     
     var intro=Ti.UI.createLabel({
         text:p.item.intro,
         width:dataHolder.width,
         font : {
            fontFamily : VARS._iOS ? 'Signika' : 'Signika-Light',
            fontSize : "11dp",
            fontStyle : 'italic'
         },
         top:"25dp",
         left:"5dp",
         right:"5dp",
         height:"50dp",
         color:VARS.textColor,
         verticalAlign:Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
         touchEnabled:false,
         index:p.item.index,
     });
     dataHolder.add(intro);
     
     var priceDevider=Ti.UI.createView({
         height:"0.3dp",
         backgroundColor:VARS.textColor,
         width:p.item.photo ? (VARS._dpiWidth-30-100)+"dp" : (VARS._dpiWidth-30)+"dp",
         bottom: "35dp",
         opacity:0.2,
         touchEnabled:false,
         index:p.item.index,
     });
     dataHolder.add(priceDevider);
     
	 var priceText = p.item.prices.length>0 ? p.item.prices[0].value : '';
	 priceText =  priceText + " " + L('currency') ; 		
		     
     var price=Ti.UI.createLabel({
          text: priceText,
         //width:dataHolder.width,
         font : {
            fontFamily : VARS._iOS ? 'Eraser' : 'EraserRegular',
            fontSize : "14dp",
            fontStyle : 'italic'
         },
         bottom:"5dp",
         right:"10dp",
         height:"30dp",
         color:"#c9892e",
         verticalAlign:Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
         touchEnabled:false,
         index:p.item.index,
     });
     dataHolder.add(price);
     
     
     if(p.item.thumb){
         Ti.API.info("Menu image "+ p.item.photo);
         var itemPhoto=Ti.UI.createImageView({
             image:p.item.thumb,
             touchEnabled:false,
             preventDefaultImage:true,
             width:"130dp",
             height:"110dp",
             borderRadius:"8dp",
             left:"10dp",
             touchEnabled:false,
             index:p.item.index,
         });
         row.add(itemPhoto);
     }
  
    //############### FUNCTIONS ###############
    //TODO ADD CODE
  
    //############### EVENT LISTENERS ###############
    //TODO ADD CODE
  
    //############### SETUP AND RETURN ###############
    row.add(dataHolder);
    return row;
}
exports.createMenuRow=createMenuRow;

/**
 * @method createItemSelector
 * Cretes items selector where user can choose type and quantity
 * @param {Object} p
 * - {Object} prices, the prices array
 * @return {Titanium.UI.View} itemSelector the view holder for the itemSelector
 */
function createItemSelector(p){
    //############### DATA ###############
    var selectedQty=1;
    var selectedIndex=0;
  
    //############### VIEWS ###############
    var itemSelector=Ti.UI.createView({
        height:"50dp",
        bottom:(VARS._iOS&&VARS._NavigationType=="tab")?"48dp":"0dp",
        backgroundColor:VARS.rowColor,
        width:"0%"
    });
    
    var firstVSeparator=Ti.UI.createView({
        height:itemSelector.height,
        width:"1dp",
        left:(VARS._dpiWidth/3)+"dp",
        backgroundColor:VARS.separatoColor
    });
  //  itemSelector.add(firstVSeparator);
    
    var secondVSeparator=Ti.UI.createView({
        height:itemSelector.height,
        width:"1dp",
        left:2*(VARS._dpiWidth/3)+"dp",
        backgroundColor:VARS.separatoColor
    });
  //  itemSelector.add(secondVSeparator);
    
    var typeLegend=Ti.UI.createLabel({
        top:"0dp",
        left:"0dp",
        width:(VARS._dpiWidth/3)+"dp",
        textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,
        height:"25dp",
        font:VARS.h4,
        color:VARS.textColor,
        text:L('type'),
        touchEnabled:false,
    });
   // itemSelector.add(typeLegend);
    
    var type=Ti.UI.createLabel({
        top:"25dp",
        left:"0dp",
        width:(VARS._dpiWidth/3)+"dp",
        textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,
        height:"25dp",
        font:VARS.h4,
        color:VARS.textColor,
        text:p.prices.length>0 ? p.prices[0].name:"",
        touchEnabled:false,
    });
   // itemSelector.add(type);
    
    var typeSelector=Ti.UI.createView({
        left:"0dp",
        height:itemSelector.height,
        width:(VARS._dpiWidth/3)+"dp",
    });
   // itemSelector.add(typeSelector);
    
    
    //Type dialog 
    var options=[];
    for(var i=0;i<p.prices.length;i++){
        options.push(p.prices[i].name+" -- "+p.prices[i].value+" "+"SR");
    }
    options.push(L('cancel'));
    
    var typeDialog = Ti.UI.createOptionDialog({
        cancel : p.prices.length,
        options : options,
        selectedIndex : 0,
        title : L('type')
    }); 

    
    var qtyLegend=Ti.UI.createLabel({
        top:"0dp",
        left:(VARS._dpiWidth/3)+"dp",
        width:(VARS._dpiWidth/3)+"dp",
        textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,
        height:"25dp",
        font:VARS.h4,
        color:VARS.textColor,
        text:L('qty'),
        touchEnabled:false,
    });
   // itemSelector.add(qtyLegend);
    
     var qty=Ti.UI.createLabel({
        top:"25dp",
        left:(VARS._dpiWidth/3)+"dp",
        width:(VARS._dpiWidth/3)+"dp",
        textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,
        height:"25dp",
        font:VARS.h4,
        color:VARS.textColor,
        text:1,
        touchEnabled:false,
    });
    //itemSelector.add(qty);
    
    var qtySelector=Ti.UI.createView({
        left:(VARS._dpiWidth/3)+"dp",
        height:itemSelector.height,
        width:(VARS._dpiWidth/3)+"dp",
    });
  //  itemSelector.add(qtySelector);
    
    
    //Type dialog 
    var optionsQty=[];
    for(var i=0;i<5;i++){
        optionsQty.push(i+1);
    }
    optionsQty.push(L('cancel'));
    
    var qtyDialog = Ti.UI.createOptionDialog({
        cancel : 5,
        options : optionsQty,
        selectedIndex : 0,
        title : L('qty')
    }); 
    
     var addSelector=Ti.UI.createView({
        right:"0dp",
        height:itemSelector.height,
        width:(VARS._dpiWidth/3)+"dp",
        backgroundColor:VARS.rowOddColor,
    });
  // itemSelector.add(addSelector);
    
    var addLegend=Ti.UI.createLabel({
        top:"0dp",
        left:(2*(VARS._dpiWidth/3)+10)+"dp",
        width:(VARS._dpiWidth/9)+"dp",
        textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,
        height:itemSelector.height,
        font:VARS.h4,
        color:VARS.textColor,
        text:L('add'),
        touchEnabled:false,
    });
//    itemSelector.add(addLegend);
    
    var cardIcon=Ti.UI.createImageView({
        image:"/images/card.png",
        right:"10dp",
        width:(VARS._dpiWidth/9)+"dp",
        touchEnabled:false,
    });
//    itemSelector.add(cardIcon);
    
    //############### FUNCTIONS ###############
    //TODO ADD CODE
  
    //############### EVENT LISTENERS ###############
    typeSelector.addEventListener('click',function(e){
        typeDialog.show();
    });
    
    qtySelector.addEventListener('click',function(e){
        qtyDialog.show();
    });
    
    addSelector.addEventListener('click',function(e){
        p.selectedQty=selectedQty;
        p.selectedIndex=selectedIndex;
        var order=Ti.App.Properties.getObject('menuOrder',[]);
        if(p.prices[selectedIndex]){
        	cardIcon.image="/images/correct.png";
        	order.push(p);
        	Ti.App.Properties.setObject('menuOrder',order);
        }else{
        	alert(L('noprices'));
        }
        Ti.API.info(Ti.App.Properties.getObject('menuOrder',[]));
    });
    
    
    typeDialog.addEventListener('click',function(e){
        if(e.index!=p.prices.length){
            type.text=p.prices[e.index].name;
            selectedIndex=e.index;
        }
       
    });
    
    qtyDialog.addEventListener('click',function(e){
        if(e.index!=5){
            qty.text=e.index+1;
            selectedQty=e.index+1;
        }
       
    });
  
    //############### SETUP AND RETURN ###############
    //TODO ADD CODE
    return itemSelector;
}
exports.createItemSelector=createItemSelector;

/**
 * @method createPriceRow
 * Creates price row for the item details
 * @param {Object} p
 * - {String} price
 * - {String} priceName
 * - {Number} top, top position
 * @return {Titanium.UU.View} priceRow The row to be displayed
 */
function createPriceRow(p){
    //############### DATA ###############
    //TODO ADD CODE
      
    //############### VIEWS ###############
    var priceRow=Ti.UI.createView({
        height:"50dp",
        top:p.top,
        width:"100%"
    });
    
    var priceDevider=Ti.UI.createView({
         height:"0.3dp",
         backgroundColor:VARS.textColor,
         width:"90%",
         top:"0dp",
         opacity:0.2
    });
     priceRow.add(priceDevider);
     
     var price=Ti.UI.createLabel({
         text:p.price+" "+ L('currency'),
         //width:dataHolder.width,
         font : {
            fontFamily : VARS._iOS ? 'Eraser' : 'EraserRegular',
            fontSize : "14dp",
            fontStyle : 'italic'
         },
         right:"10dp",
         height:"50dp",
         color:"#c9892e",
         verticalAlign:Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER
     });
     priceRow.add(price);
     
     
     
     var priceName=Ti.UI.createLabel({
         text:p.priceName,
         //width:dataHolder.width,
         font : {
            fontFamily : VARS._iOS ? 'Signika' : 'Signika-Light',
            fontSize : "14dp",
            fontStyle : 'italic'
         },
         left:"10dp",
         height:"50dp",
         color:VARS.textColor,
         verticalAlign:Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER
     });
     priceRow.add(priceName);
     
    
    

  
    //############### FUNCTIONS ###############
    //TODO ADD CODE
  
    //############### EVENT LISTENERS ###############
    //TODO ADD CODE
  
    //############### SETUP AND RETURN ###############
    //TODO ADD CODE
    return priceRow;
}
exports.createPriceRow=createPriceRow;


/**
 * @method createOrderRow
 * Creates price row for the item details
 * @param {Object} p
 * - {String} price
 * - {String} itemName
 * - {String} orderDetails
 * - {Number} top, top position
 * - {Number} index
 * @return {Titanium.UU.View} priceRow The row to be displayed
 */
function createOrderRow(p){
    //############### DATA ###############
    //TODO ADD CODE
      
    //############### VIEWS ###############
    var priceRow=Ti.UI.createView({
        height:"50dp",
        top:p.top,
        width:"100%",
        className:"row",
        index:p.index
    });
    
    var priceDevider=Ti.UI.createView({
         height:"0.3dp",
         backgroundColor:VARS.textColor,
         width:"90%",
         top:"0dp",
         opacity:0.2,
         touchEnabled:false,
    });
     priceRow.add(priceDevider);
     
     var price=Ti.UI.createLabel({
         text:p.price+" "+Ti.App.Properties.getObject('settings').config.currency||"SR",
         //width:dataHolder.width,
         font : {
            fontFamily : VARS._iOS ? 'Eraser' : 'EraserRegular',
            fontSize : "14dp",
            fontStyle : 'italic'
         },
         right:"10dp",
         height:"50dp",
         color:VARS.barColor,
         verticalAlign:Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
         touchEnabled:false,
     });
     priceRow.add(price);
     
     
     var itemName=Ti.UI.createLabel({
         text:p.itemName,
         //width:dataHolder.width,
         font : {
            fontFamily : VARS._iOS ? 'Signika' : 'Signika-Light',
            fontSize : "14dp",
            fontStyle : 'italic'
         },
         left:"30dp",
         height:"40dp",
         top:"0dp",
         color:VARS.textColor,
         verticalAlign:Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
         touchEnabled:false,
     });
     priceRow.add(itemName);
     
     var orderDetails=Ti.UI.createLabel({
         text:p.orderDetails,
         left:"30dp",
         //width:dataHolder.width,
         font : {
            fontFamily : VARS._iOS ? 'Signika' : 'Signika-Light',
            fontSize : "11dp",
            fontStyle : 'italic'
         },
         height:"25dp",
         bottom:"0dp",
         color:VARS.textColor,
         verticalAlign:Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
         touchEnabled:false,
     });
     priceRow.add(orderDetails);
     
     var trushIcon=Ti.UI.createImageView({
         width:"20dp",
         image:"/images/trush.png",
         left:"5dp",
         touchEnabled:false,
     });
     priceRow.add(trushIcon);
     
     var trashSelector=Ti.UI.createView({
         width:"30dp",
         height:"50dp",
         left:"0dp",
         className:"trush",
         index:p.index
     });
     priceRow.add(trashSelector);
    
    

  
    //############### FUNCTIONS ###############
    //TODO ADD CODE
  
    //############### EVENT LISTENERS ###############
    //TODO ADD CODE
  
    //############### SETUP AND RETURN ###############
    //TODO ADD CODE
    return priceRow;
}
exports.createOrderRow=createOrderRow;


exports.buttonOrderNowBottom = function() {

	var view = Ti.UI.createView({
		backgroundColor: "#2d8bbe",
		width: Ti.Locale.currentLanguage=='ar' ? "90dp"  : "130dp",
		height: "40dp",
		borderRadius: 5, 
		layout: "horizontal"
	});
	var marginSpace = Ti.UI.createView({
		width: "10dp"
	});
	view.add(marginSpace);
	
	var label = Ti.UI.createLabel({
		text: L('order_now'),
		color: '#ffffff',
		font: {
			fontFamily : VARS._iOS ? 'Signika' : 'Signika-Light',
            fontSize : "13dp"
		}
	});
	view.add(label);
	var space = Ti.UI.createView({
		width: "5dp"
	});
	view.add(space);
	
	var sep = Ti.UI.createImageView({
		image: '/images/separator.png',
		height: "34dp"
	});
	view.add(sep);
	
	var space_02 = Ti.UI.createView({
		width: VARS._platform == VARS._android  ? "5dp" : 0
	});
	view.add(space_02);
	
	var imgCart = Ti.UI.createImageView({
		image: '/images/shoppingfill.png',
		height: "30dp"
		
	});
	view.add(imgCart);
	
	return view;
};
    