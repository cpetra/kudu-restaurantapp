
getDeletedContent = function() {
	var sendit = Ti.Network.createHTTPClient({ 
         onerror: function(e){ 
               Ti.API.info(e.error);
               //alert('There was an error during the connection item'); 
         }, 
      	timeout:10000, 
	});
	var url = 'http://m-kudu.h.edesigner-test.com/deleted';
	sendit.open('GET', url);  
	sendit.send(); 
	sendit.onload = function(){ 
		var jsonDelete = JSON.parse(this.responseText) ;
		deleteContent( jsonDelete['deleted-content']  );   
	 };   
};

deleteContent = function(param) {
	deleteMenuCategories(param);
	deleteMenuItems(param);
};

deleteMenuCategories = function(param) {
	var db = Titanium.Database.open('menucategory_db');
	var sql = 'DELETE FROM menucategory_tb WHERE id=' ;
	var categories = param;

	for(var i=0; i<categories.length; i++){
		var category = categories[i].content;
		db.execute(sql + '' + category.id); 
	}
	db.close();
};

deleteMenuItems = function(param) {
	var db = Titanium.Database.open('menucategory_db');
	var sql = 'DELETE FROM menuitems_tb WHERE id=';
	var categories = param;
	for(var i=0; i<categories.length; i++){
		var category = categories[i].content;
		db.execute(sql + '' + category.id); 
	}
	db.close();
};

exports.getDeletedContent = getDeletedContent;
