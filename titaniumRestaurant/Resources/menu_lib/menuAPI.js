/**
 * Menu Module for Mobidonia
 * @author Daniel Dimov, <dimovdaniel@yahoo.com>
 */

/**
 * @property Cloud
 * @type Titanium.Cloud
 */
var Cloud = require('ti.cloud');
Cloud.debug = true;

/**
 * @property VARS
 * @type Settings
 */

var VARS = require('common/globals');
var databaseAPI = require('menu_lib/databaseAPI');
var updateAPI = require('menu_lib/updateAPI');
var deleteAPI = require('menu_lib/deleteAPI');
var menucategory_db;

createTableMenuCategory = function() {
	//create table only the first time
	if(Titanium.App.Properties.getBool('menuCategoryTbCreated'))																		
		return;
		
	var menuCategoryFile = Ti.Filesystem.getFile(Ti.Filesystem.resourcesDirectory,'json/menuCategory.json');	    
	var preParseData = (menuCategoryFile.read().text); 
	var response = JSON.parse(preParseData);
	
	var jsonCategories = (response);
	Titanium.API.info(jsonCategories);
    Titanium.App.Properties.setBool('menuCategoryTbCreated', true);
    populateTableMenuCategoryFirstTime(jsonCategories);
	
};


function createTableMenuItems() {
	//create table only the first time
	if(Titanium.App.Properties.getBool('menuItemsTableCreated'))
		return;
		
	var menuItemsFile = Ti.Filesystem.getFile(Ti.Filesystem.resourcesDirectory,'json/menuItems.json');	    
	var preParseData = (menuItemsFile.read().text); 
	var response = JSON.parse(preParseData);
	
	var jsonMenuItems = (response);
	Titanium.API.info(jsonMenuItems);
    Titanium.App.Properties.setBool('menuItemsTableCreated', true);
    populateTableMenuItemsFirstTime(jsonMenuItems);
	
}

function updateTableMenuItems() {
	getChanges("http://m-kudu.h.edesigner-test.com/content-updated", getLastSyncItems(), function(e){
		var changes = e.changes['updated-content'];
		applyChanges(changes);
	});
	
};

function deleteTableItems() {
	getDeletedContent();	
}

Titanium.App.addEventListener('applicationHomeLOADED', function(e){	
	Titanium.API.info('MenuApi---applicationHomeLOADED');
	//if( !checkInternetConnection() )
	//	return;
});
                               
createTableOnApplicationStarted = function() {
	Titanium.API.info('aaappp started');	
	
	createTableMenuCategory();
	
	//create table if not created or updated table
	if( (Titanium.App.Properties.getBool('menuItemsTableCreated')) )	{
		updateTableMenuItems();
		//delete items
		deleteTableItems();
	} else {
		createTableMenuItems();
	}
		
};

exports.createTableOnApplicationStarted = createTableOnApplicationStarted;

function checkInternetConnection() {
	
	if (Titanium.Network.networkType === Titanium.Network.NETWORK_NONE) {
		return false;
	} else {
	   return true;
	}
	
}

/**
 * @method fetchCloudMenuCategories
 * Fetch categories from Ti Cloud
 * @param {Object} param
 * - {Function} listener Listener for the result
 * @return ArrayList of categories
 */
fetchCloudMenuCategories = function(param) {
    //List of categories
    var categories = [];
    
	menucategory_db = Titanium.Database.open('menucategory_db');
	var rows = menucategory_db.execute('select * from menucategory_tb ORDER BY weight');
	var i = 0;
	while (rows.isValidRow()){
	  Ti.API.info(rows);
	  Ti.API.info('menucategory ---> cid: ' + rows.fieldByName('id') + ', title:' + rows.fieldByName('title') + ', image: ' + rows.fieldByName('image') );
	  var category = rows;
	  categories.push({
            id : category.fieldByName('id'),
            title : category.fieldByName('title'),
            photo : category.fieldByName('image'),
            arabic_title: category.fieldByName('arabic_title'),
            weight: category.fieldByName('weight'),
            index : i,   			  	
	  });
	  i++;
	  rows.next();
	}
	rows.close();
	menucategory_db.close();
	
	param.listener(categories);
	
	return;
	
    
};
    
exports.fetchCloudMenuCategories=fetchCloudMenuCategories;

/**
 * @method fetchCloudMenu
 * Fetch menu from Ti Cloud
 * @param {Object} param
 * - {Function} listener Listener for the result
 * - {String} categoryID
 * @return ArrayList of menu items
 */
fetchCloudMenu = function(param) {
    //List of menu items
    var items = [];
	
	menucategory_db = Titanium.Database.open('menucategory_db');
	Titanium.API.info("categoryID:" + param.categoryID);
	if(param.categoryID == undefined)
		return;
	var rows = menucategory_db.execute('SELECT * FROM menuitems_tb  WHERE cid=' + param.categoryID  + ' ' + 'ORDER BY weight');
	var i = 0;
	Titanium.API.info('fetch Cloud Menu');
	while (rows.isValidRow()){
	  Ti.API.info('item ---> cid: ' + rows.fieldByName('cid') + ', title:' + rows.fieldByName('title') + ', updated: ' + rows.fieldByName('updated')  + ', arabic_title: ' + rows.fieldByName('arabic_title'));
	  var item = rows;
      var priceTemp = [{
      	"name": "",
      	"value": item.fieldByName('price')
      }];
    
	  items.push({
            id : item.fieldByName('cid'),
            title : item.fieldByName('title'),
            photo : item.fieldByName('image'),
            thumb: item.fieldByName('thumb'),
            //intro: item.fieldByName('intro'),
            content: '',
            description:  item.fieldByName('description'),
            arabic_description: item.fieldByName('arabic_description'),
            prices: priceTemp,
            updated: item.fieldByName('updated'),
            arabic_title: item.fieldByName('arabic_title'),
            arabic_url: item.fieldByName('arabic_url'),
            english_url: item.fieldByName('english_url'),
            weight: item.fieldByName('weight'),
            index : i,   			  	
	  });
	  i++;
	  rows.next();
	}
	rows.close();
	menucategory_db.close();
	
	//Get category ordering
    var itemsOrdering = VARS.getProperty(param.categoryID, 'resmenupositions');
    if (itemsOrdering) {
        //Joint the sections
        for (var i = 0; i < itemsOrdering.length; i++) {
            for (var j = 0; j < items.length; j++) {
                if (itemsOrdering[i].id == items[j].id) {
                    itemsOrdering[i] = items[j];
                     items[j].used=true;
                    continue;
                }
            }
        }

        //Fix empty sections(Prevents bugs)
        for (var i = 0; i < itemsOrdering.length; i++) {
            if (!itemsOrdering[i].title) {
                //If sections doesn't have window, remove it.
                itemsOrdering.splice(i, 1);
            }
        }
        //Add categories that are not included in the menu
        for (var j = 0; j < items.length; j++) {
                if (!items[j].used) {
                    itemsOrdering.push(categories[j]);
                }
            }
        items = itemsOrdering;
    } 
	
	param.listener(items);	
	return;
	
};
exports.fetchCloudMenu = fetchCloudMenu;


/**
 * @method submitOrder
 * Submit order to acs
 * @param {Object} param
 * - {Function} listener Listener for the result
 * - {Object} order
 */
submitOrder = function(param) {
    Cloud.Objects.create({
        classname : 'menuorder',
        fields :param.order
    }, function(e) {
        if (e.success) {
            var menuorder = e.menuorder[0];
            Ti.API.info("ID:"+menuorder.id);           
        } else {
            Ti.API.info('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
        }
        
    });
};
exports.submitOrder=submitOrder;


