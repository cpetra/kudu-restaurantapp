/*
* @author DANIEL DIMOV
* @email dimovdaniel@yahoo.com
* @content news Module
* @version 3.0
* 
* @changes
* ---- Version 3.0 ------
* Add adMob for android
* More powerful algorith for finding images in wordpress post, now accepts post feature image
*/
var VARS =require('common/globals');
var GUI_news=require('news_lib/newsGUI');
var API_news=require('news_lib/newsAPI');
var Facebook = require('facebook');
if(VARS.displayAdMobAdsNews){var ADS=require('common/ads');} //AD LIBRARY
var activeArticle=null;

/**
 * @desc Creates list of news
 * @param {Object} p
 * @config {Number} type, type of News source to use [_ACS]
 * @config {View} win, the window that news should be displayed in
 * @config {View} tab, the tab to display the window in
 */
exports.createNewsList=function(p)
{
	
	
	//####################### DATA ###################
	var posts=[]; //List of posts/articles/tweets/items
	var type=p.type;
	var url=p.url;
	var page=1; //Current page
	
	var pulling = false;
	var reloading = false;
	var offset = 0;
	var loading=true;
	
	//###################### VIEWS ###################
	//AD
	//newsListHeight="100%";
	newsListBottom="0dp";
	newsListTop=null;
	if(VARS.displayAdMobAdsNews)
	{
		var theAd=ADS.requestAd({win:p.win});
		newsListBottom=VARS.androidAdHeight+"dp";
		//newsListHeight=null;
	}
		
	var newsList=Ti.UI.createTableView({
		top:"0dp",
		width:"100%",
		top:newsListTop,
		bottom:newsListBottom,
		//height:newsListHeight,
		backgroundColor : VARS.windowBackgroundColor,
		separatorColor : VARS.separatoColor,
	})
	
	if(VARS._platform==VARS._android)
	{
		var androidActInd = Titanium.UI.createActivityIndicator({
			bottom : "10dp",
			width : "60dp",
			height : "60dp",
		});
	}
	
	
	
	
	if (VARS._iOS) {
		var categorisButton = Ti.UI.createButton({
			style : Ti.UI.iPhone.SystemButtonStyle,
			title : L('categories')
		})

		var optionsButton = Ti.UI.createButton({
			style : Ti.UI.iPhone.SystemButtonStyle,
			title : L('options')
		})

		var refreshButton = Ti.UI.createButton({
			systemButton : Titanium.UI.iPhone.SystemButton.REFRESH
		})

		var indicatorButton = Ti.UI.createButton({
			systemButton : Titanium.UI.iPhone.SystemButton.SPINNER
		})

		p.win.rightNavButton = indicatorButton;
	}

	
	//In iOS create the reloader header
	if(VARS._iOS)
	{
		var tableHeader=Ti.UI.createView({
			backgroundColor:VARS.reloadBackgroundColor,
			width:VARS._dpiWidth,
			height:60
		});
		
		var border=Ti.UI.createView({
			backgroundColor:VARS.reloadTextColor,
			bottom:0,
			height:2
		})
		tableHeader.add(border);
		
		
		var imageArrow=Ti.UI.createImageView({
			image:"pull.png",
			left:10,
			bottom:10,
			height:60,
		})
		tableHeader.add(imageArrow);
			
		var labelStatus=Ti.UI.createLabel({
			text:L('pulltoreload'),
			left:65,
			bottom:10,
			height:60,
			font:VARS.h2,
			color:VARS.reloadTextColor
		})
		tableHeader.add(labelStatus);
		
		var actInd=Ti.UI.createActivityIndicator({
			left:15,
			style:Titanium.UI.iPhone.ActivityIndicatorStyle.DARK,
			bottom:24,
			width:30,
			height:30,
		})
		tableHeader.add(actInd);
		
		//Assign the header
		newsList.headerPullView=tableHeader;
		
		
		
		
	}
	
	//####################### FUNCTIONS ##############
	function setPosts(thePosts)
	{
		loading=false;
		if(VARS._iOS)
		{
			//Add link to the twitter account
			if(p.type == VARS._TW)
			{
				var accountLink=Ti.UI.createButton({
					systemButton:Ti.UI.iPhone.SystemButton.ACTION
				})
				accountLink.addEventListener('click',function(e){
					openTwitterAccount();
				})
				p.win.rightNavButton=accountLink;
			}
			else
			{
				p.win.rightNavButton=null;
			}
		}
		
		if(page==1){
		    posts=[];
		}		
		for(var i=0;i<thePosts.length;i++)
		{
			posts.push(GUI_news.creteSimpleRow(thePosts[i]));
		}
		newsList.setData(posts);
		
		//If twitter and if page is 1, set up the banner
        if(page==1&&p.type==VARS._TW&&thePosts.length>0&&thePosts[0].userObject)
        {
            p.win.add(GUI_news.userBannerCreator({
                user : thePosts[0].userObject,
            })); 

        }
		
		
		newsList.setData(posts);
		if(VARS._iOS)
		{
			resetPullHeader(newsList);
		}
		else
		{
			androidActInd.hide();
		}
        
	}
	
	//Method to call specific api
	function callToApi() 
	{
		loading=true;
		//Start fetching items
		if (p.type == VARS._ACS) 
		{
			//Fetch RSS items
			API_news.fetchCloudPosts({
				page:page,
				count:25,
				listener : setPosts,
			})
		} 
		
		//Display indicator in android
		if(VARS._platform==VARS._android&&page==1)
		{
			androidActInd.show();
		}
	}
	callToApi();

	
	
	
	//################# EVENT LISTENERS ##############
	//Reload table
	if (VARS._iOS) {
		newsList.addEventListener('scroll', function(e) 
		{
			offset = e.contentOffset.y;
			if (pulling && !reloading && offset > -80 && offset < 0) {
				pulling = false;
				var unrotate = Ti.UI.create2DMatrix();
				imageArrow.animate({
					transform : unrotate,
					duration : 180
				});
				labelStatus.text = L('pulltoreload');
			} 
			else if (!pulling && !reloading && offset < -80) 
			{
				pulling = true;
				var rotate = Ti.UI.create2DMatrix().rotate(180);
				imageArrow.animate({
					transform : rotate,
					duration : 180
				});
				labelStatus.text = L('releasetorefresh');
			}
		});

		function resetPullHeader(table) {
			reloading = false;
			actInd.hide();
			imageArrow.transform = Ti.UI.create2DMatrix();
			imageArrow.show();
			labelStatus.text = L('pulltoreload');
			table.setContentInsets({
				top : 0
			}, {
				animated : true
			});
		}


		newsList.addEventListener('dragEnd', function(e) {
			if (pulling && !reloading && offset < -80) {
				pulling = false;
				reloading = true;
				labelStatus.text = L('updating');
				imageArrow.hide();
				actInd.show();
				e.source.setContentInsets({
					top : 80
				}, {
					animated : true
				});
				page=1;
				callToApi();
			}
		});
	}

	//Click on the table
	newsList.addEventListener('click',function(e)
	{
			//This is click on element
			if(e.source.data.type==VARS._TW&&VARS._directLinkToTwitter)
			{
				Ti.Platform.openURL(e.source.data.link);
			}
			else
			{
				//if(!p.win.containingTab){p.win.containingTab=}
				p.win.containingTab.open(createArticleDetails(e.source.data));
			}
		
		
		
	})
	
	if (VARS._platform == VARS._android) {
		p.win.activity.onCreateOptionsMenu = function(e) {
					var menu = e.menu;
					if(p.type == VARS._TW){
					    var menuItemOpenintwitter = menu.add({
                            title : L('openintw'),
                            showAsAction : Ti.Android.SHOW_AS_ACTION_ALWAYS,
                            icon : "/images/action_about.png",
                        });
                        menuItemOpenintwitter.addEventListener("click", function(e) {
                            openTwitterAccount();
                        });
					}
					
					var menuItemReload = menu.add({
						title : L('reload'),
						showAsAction : Ti.Android.SHOW_AS_ACTION_ALWAYS,
                        icon : "/images/navigation_refresh.png"
					});
					
					menuItemReload.addEventListener("click", function(e) {
						page=1;
						callToApi();
					});
				};
	}
	
	
	//Add the list to the window
	p.win.add(newsList);
	
	if(VARS._platform==VARS._android)
	{
		p.win.add(androidActInd);
	}
	

}



var startFBShare=function()
        {
        	if(Facebook.loggedIn) 
				{
					//User is loged in, just share the image
					updateStatus();

				} 
				else 
				{
					//We need to login the user for firs time
					Facebook.addEventListener('login', updateStatus);
					Facebook.authorize();
				}
        }

var updateStatus = function() 
{
	//Check once again if logged in
	if(Facebook.loggedIn) 
	{
		var data = {
			link: activeArticle.link,
			name: activeArticle.title,
			message: activeArticle.text,
		};
		Facebook.requestWithGraphPath('me/feed', data, 'POST', showRequestResult);
	}

}

function showRequestResult(e) {
		var s = '';
		if (e.success) {
			s=L('fbshared');
		} else if (e.cancelled) {
			s=L('fbnotshared');
		} else {
			s=L('fbnotshared');
		}
		alert(s);
	}    
/**
 * @method send_mail
 * @param p Parameters
 * - {String} title, share title
 * - {String} link, link to the article
 * - {String} content, the rest of the content in the email
 */
function send_mail(p) {
	var emailDialog = Titanium.UI.createEmailDialog();
	if (!emailDialog.isSupported()) {
		Ti.UI.createAlertDialog({
			title : L('error'),
			message : L('emailnotavailable')
		}).show();
		return;
	}
	emailDialog.setSubject(p.title);
	emailDialog.setHtml(true);
	emailDialog.setMessageBody(L('sharearticle')+": <a href=\""+p.link+"\">"+p.link+"</a>"+"<br />"+p.content);

	emailDialog.addEventListener('complete', function(e) {
		if (e.result == emailDialog.SENT) {
			if (Ti.Platform.osname != 'android') {
				// android doesn't give us useful result codes.
				// it anyway shows a toast.
				alert(L('messagesend'));
			}
		} else {
			alert(L('messagenotsend'));
		}
	});
	emailDialog.open();
}




/**
 * 
 * @param {Object} article
 */
var createArticleDetails=function(article)
{
	
	//######################## DATA ################
	var articleHTML="<html>";
	var meta_and_style='<link rel="stylesheet" href="http://twitter.github.com/bootstrap/1.4.0/bootstrap.min.css">';
	    meta_and_style+="<style>img { display:block;  max-width: 300px; height:auto;} hr {margin-bottom:0;} h2 {margin-bottom:0;} h5 { margin-bottom:10px;}</style>"
	    meta_and_style+="<style>body { margin: 10px;}</style>";
	    meta_and_style+="<style>pubDate {bisibility:hidden}</style>"
	var title='<h2>'+article.title+'</h2>';
	var date='<h5>'+article.created_at+"</h5>";
	articleHTML=meta_and_style+"<body>"+title;
    if(!article.type==VARS._TW)
	{
		articleHTML+=date+"<hr />";
	}

	
	articleHTML+=article.content;
	
	if(article.type==VARS._TW)
	{
		articleHTML+='<br/> by <a href="https://www.twitter.com/'+article.user+'">'+article.user+'</a>';
	}
	else
	{
		//Add read article 
		//articleHTML+='<br /><a href="'+article.link+'">'+L('source')+'</a><br />'
	}
	
	articleHTML+="</body></html>"
	
	//######################## VIEWS ##############
	
	var articleDetailsWindow = Ti.UI.createWindow({
	    title:article.title,
		backgroundColor : VARS.windowBackgroundColor,
		navBarHidden : false,
		barColor : VARS.barColor,
		scalesPageToFit:true,
		enableZoomControls:false,
		horizontalWrap:true,
		extendEdges : [],
        orientationModes : [Titanium.UI.PORTRAIT],
        translucent : false,
        navTintColor : VARS.tabTintColor,
        tintColor:VARS.tabTintColor,
		
	})
	articleWebHeight="100%";
	articleWebWidth="100%";
	articleWebBottom="0dp";
	if(VARS.displayAdMobAdsNews)
	{
		var theAd=ADS.requestAd({win:articleDetailsWindow});
		articleWebHeight=VARS._dpiHeight-VARS.AdHeightDisplayed;
		articleWebBottom=VARS.AdHeightDisplayed;
		articleWebHeight=null;
	}
	if(VARS._platform==VARS._android)
	{
		//articleWebWidth=VARS._dpiWidth
	}
	
	var articleWeb = Ti.UI.createWebView({
			top:0,
            height: articleWebHeight,
            bottom: articleWebBottom,
            width: articleWebWidth,
            html: articleHTML,
        });
        
    var shareButton=Ti.UI.createButton({
    	style:Ti.UI.iPhone.SystemButtonStyle,
    	title:L('share'),
    })
    
    var shareDialog=Ti.UI.createOptionDialog({
    	options:[L('facebook'),L('emailshare'),L('cancel')],
    	cancel:2,
    	title:L('share')
    })
    
    shareButton.addEventListener("click",function(e){
    	shareDialog.show();
    })
    
    shareDialog.addEventListener('click',function(e){
    	if(e.index==0)
    	{
    		activeArticle=article;
    		startFBShare();
    	}
    	else if(e.index==1)
    	{
    		send_mail({
    		    title:article.title,
    		    link:article.link,
    		    content:article.content
    		});
    	}
    })
        
    articleDetailsWindow.add(articleWeb);
    if(VARS._iOS){
    	articleDetailsWindow.setRightNavButton(shareButton);
    }
   
   //In android add menu items
	if(VARS._platform==VARS._android)
	{
		articleDetailsWindow.activity.onCreateOptionsMenu = function(e){
  			var menu = e.menu;
  			
            var menuItemShare = menu.add({
                title : L('share'),
                showAsAction : Ti.Android.SHOW_AS_ACTION_ALWAYS,
                icon : "/images/social_share.png"
            }); 

  			menuItemShare.addEventListener("click", function(e) 
  			{
  					shareDialog.show();
    					
  			});
		};
	}
			
    return articleDetailsWindow;
}
