var VARS =require('common/globals'); //Here we store all global variables / settings



function ApplicationWebWindow(p) {
	var self = Ti.UI.createWindow({
	    title:p.title,
		backgroundColor : VARS.windowBackgroundColor,
		navBarHidden : false,
		barColor : VARS.barColor,
		orientationModes : [Titanium.UI.PORTRAIT]
	});
	
	var webview = Titanium.UI.createWebView({
		url : p.url,
		data:p.data,
	});
	self.add(webview);

	
	
	return self;
};

module.exports = ApplicationWebWindow;
