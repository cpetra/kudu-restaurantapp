var VARS =require('common/globals'); //Here we store all global variables / settings


function ApplicationWindow(title) {
    var bgImage="/images/bg.png";
    if(VARS._platform==VARS._iPad){
         bgImage="/images/ipad/bg.png";
    }
	var self = Ti.UI.createWindow({
	    title:title,
		backgroundColor : VARS.windowBackgroundColor,
		//backgroundImage:bgImage,
		navBarHidden : false,
	    barColor : VARS.barColor,
		extendEdges : [],
        orientationModes : [Titanium.UI.PORTRAIT],
        translucent : false,
        navTintColor : VARS.tabTintColor,
        tintColor:VARS.tabTintColor,
	});
	
	
	
	return self;
};

module.exports = ApplicationWindow;
