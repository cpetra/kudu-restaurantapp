//##################### DATA ##################
var VARS =require('common/globals'); //Here we store all global variables / settings
var GUI =require('common/gui'); //Here we store all global variables / settings
var locale = require('com.shareourideas.locale');
//if(VARS.displayAdMobAdsNavigation){var ADS=require('common/ads');} //AD LIBRARY

/**
 * Create the icon
 * @param {Object} p
 * @config {Number} itemWidth
 * @config {Number} itemHeight
 * @config {Number} itemInsideWidth
 * @config {Number} itemInsideHeight
 * @config {Number} currentRow
 * @config {Number} currentCol
 * @config {Number} itemSeparation
 * @config {Object} section
 */
function createIcon(p) {
    
    var fontForIcon=[];
    fontForIcon[3]="13dp";
    fontForIcon[4]="12dp";
    
    //Specif modification for android
    /*if(VARS._platform==VARS._android){
        fontForIcon[3]=fontForIcon[3]*(Ti.Platform.displayCaps.logicalDensityFactor/2);
        fontForIcon[4]=fontForIcon[4]*(Ti.Platform.displayCaps.logicalDensityFactor/2);
    }*/
    
     if(VARS._platform==VARS._iPad){
        fontForIcon[3]="18dp";
        fontForIcon[4]="17dp";
    }
    
    var topIndex=[];
    topIndex[2]=1;
    topIndex[3]=0.8;
    topIndex[4]=2;
    topIndex[5]=1.5;
    
    
     var additionalSpace=0;
     if(VARS._platform==VARS._android){
         additionalSpace=(p.itemSeparation/2);
     }
     var iconHolder=Ti.UI.createView({
        height:p.itemHeight+"dp",
        width:p.itemWidth+"dp",
        left:(p.itemWidth*p.currentCol)+additionalSpace+"dp",
        top:(p.itemHeight*p.currentRow)+"dp",
    });
   
   // iconHolder.add(iconHolderSmart);
    iconHolder.win=p.section.win;
    
     var iconInsideHolder=Ti.UI.createView({
        height:p.itemInsideHeight+"dp",
        width:p.itemInsideWidth+"dp",
        touchEnabled:false,
        bubbleParent:true,
       
    });
     var iconHolderSmart=GUI.smartView({
        config:{
        height:p.itemInsideHeight+"dp",
        width:p.itemInsideWidth+"dp",
        touchEnabled:false,
    }
    });
    iconHolderSmart.opacity=0.8;
    iconHolderSmart.touchEnabled=false;
    
    iconInsideHolder.add(iconHolderSmart);
    
    Ti.API.info("itemSeparation:" + p.itemInsideWidth+  "   "+ p.itemSeparation);
    //p.itemSeparation = p.itemSeparation/1.5;
    
    var icon=Ti.UI.createImageView({
        width: VARS._platform==VARS._iPad ? p.itemInsideWidth-60-(p.itemSeparation)+"dp" : p.itemInsideWidth-(p.itemSeparation*1.1)+"dp",
        top: (p.itemSeparation*topIndex[VARS.sliderMenuColumns]-17)+"dp",
        image: "/images/metro/"+p.section.icon,
        touchEnabled:false,
    });
    
   /*
   var icon=Ti.UI.createImageView({
        width:32,
        top:(p.itemSeparation*topIndex[VARS.sliderMenuColumns])+"dp",
        image:"/images/metro/"+p.section.icon,
        touchEnabled:false,
    })
    */
    iconInsideHolder.add(icon);
   
    //VARS.sliderMenuColumns
    var theFont=fontForIcon[VARS.sliderMenuColumns];//+"dp";
    if(!fontForIcon[VARS.sliderMenuColumns]){
        theFont="11dp";
    }
    Ti.API.info("Font size:"+theFont);
    var iconName=Ti.UI.createLabel({
            text:p.section.title,
            textAlign:"center",
            bottom:"5dp",
            font:{fontSize:theFont},
            //width:p.itemWidth+"dp",
            color:VARS.textSecondColor,
            touchEnabled:false,
       });
       iconInsideHolder.add(iconName);
        
    iconHolder.add(iconInsideHolder);
    return iconHolder;
}

/**
 * Create single page, Slider
 * @param {Object} p
 * @config {Object} pageInfo
 * @config {Object} sections
 * @config {Number} optimumItemsPerPage
 * @config {Number} itemWidth
 * @config {Number} itemHeight
 * @config {Number} itemInsideWidth
 * @config {Number} itemInsideHeight,
 * @config {Number} itemSeparation
 * @config {Tab} tab
 */
function createSlider(p){
    var sliderHeight=(p.pageInfo.numRows*p.itemHeight);
    Ti.API.info("Create Slider:"+p.pageInfo.page+" with height of "+sliderHeight);
    //Main Holder
    var slider=Ti.UI.createView({
        height:sliderHeight+"dp",
        bottom:"0dp",
        width:(VARS._dpiWidth-p.itemSeparation)+"dp",
    });
    Ti.API.info("Slide width:"+(VARS._dpiWidth-p.itemSeparation)+"dp");
    for(var i=0;i<p.pageInfo.numRows;i++){
        for(var j=0;j<p.pageInfo.numCol;j++){
            
            var itemsLeft=p.pageInfo.count-(i*p.pageInfo.numCol)-(j+1);
            if(itemsLeft>=0){
                var sectionIndex=p.pageInfo.page*p.optimumItemsPerPage+(i*p.pageInfo.numCol)+j;
                //Ti.API.info(p.sections[sectionIndex].title);
                p.sections[sectionIndex].win.containingTab=p.tab;
                var item=createIcon({
                    itemWidth:p.itemWidth,
                    itemInsideWidth:p.itemInsideWidth,
                    itemHeight:p.itemHeight,
                    itemInsideHeight:p.itemInsideHeight,
                    currentRow:i,
                    currentCol:j,
                    section:p.sections[sectionIndex],
                    itemSeparation:p.itemSeparation,
                });
                item.addEventListener('click',function(e)
                {
                    p.tab.open(e.source.win);
                }); 
                slider.add(item);
                
            }
        }
    }
    return slider;
}

function ApplicationSlider(sections)
{
    
    var numRows=VARS.sliderMenuRows;
    var numCol=VARS.sliderMenuColumns;
    
    //Calculate the number of pages
    var numSections=sections.length;
    Ti.API.info("Number of sections:"+numSections);
      
    var numPages=Math.ceil(numSections/(numRows*numCol));
    Ti.API.info("Number of pages:"+numPages);
    
    var itemSeparation=Math.ceil(((VARS._dpiWidth/(numCol+2)))/(numCol+1))+10;
    var itemWidth=Math.ceil(VARS._dpiWidth/(numCol))-(itemSeparation/(numCol));
    var itemInsideWidth=itemWidth-itemSeparation;
    
    var changer=0;
    if(numCol>3){
        changer=1;
    }
    
    var itemHeight=300*(1/(numCol-changer));
    if(VARS._platform==VARS._iPad){
        itemHeight=600*(1/(numCol-changer));
    }else if(VARS._platform==VARS._android){
        itemHeight=VARS._dpiWidth*(1/(numCol-changer));
    }
    Ti.API.info("itemSeparation:"+itemSeparation);
    Ti.API.info("itemWidth:"+itemWidth);
    Ti.API.info("itemInsideWidth:"+itemInsideWidth);
    Ti.API.info("itemHeight:"+itemHeight);
    
    var pagesInfo=[]; //Contains the info about pages and their elements
    for(var i=0;i<numPages;i++){
        var count=numSections-(i*numRows*numCol); //Sections count in this page
        var rows=numRows;
        if(count>numRows*numCol){
            count=numRows*numCol;
        }else{
            rows=Math.ceil(count/numCol);
        }
        pagesInfo[i]={
            numRows:rows,
            numCol:numCol,
            count:count,
            page:i,
        };
    };
    Ti.API.info("Pages info");
    Ti.API.info(pagesInfo);
    
    //create module instance
    var tgMetroHolder = Ti.UI.createTabGroup();
    var bgImage="/images/bg.png";
    
    if(VARS._platform==VARS._iPad){
         bgImage="/images/ipad/bg.png";
    }
    
    var self = Ti.UI.createWindow({
        exitOnClose:true,
        title:VARS._AppName,
        backgroundColor : VARS.windowBackgroundColor,
      //  backgroundImage:bgImage,
        navBarHidden : true,
        tabBarHidden : true,
        barColor : VARS.barColor,
        orientationModes : [Titanium.UI.PORTRAIT],
    });
    
    var tabMain = Ti.UI.createTab({
        window: self
    });
    
    tabNav = tabMain;
    
    //AD
    SliderBottom=0;
    /*
    if(VARS.displayAdMobAdsNavigation) {
        var theAd=ADS.requestAd({win:self});
        SliderBottom=VARS.AdHeight;
    }
    */
    
    //Main Scroller
    var sliderScroll=Ti.UI.createScrollableView({
        bottom:SliderBottom,
        width:"100%",
        height:(pagesInfo[0].numRows*itemHeight+20)+"dp",
        showPagingControl:true,
        //backgroundColor:"red",
        pagingControlHeight:20,
        pagingControlAlpha:1,
        pagingControlColor:"#00000000"
    });
    
    for(var i=0;i<numPages;i++){
       sliderScroll.addView(createSlider({
          pageInfo: pagesInfo[i],
          sections: sections,
          optimumItemsPerPage:numRows*numCol,
          itemWidth:itemWidth,
          itemInsideWidth:itemInsideWidth,
          itemHeight:itemHeight,
          itemInsideHeight:itemHeight-itemSeparation,
          itemSeparation:itemSeparation,
          tab:tabMain,
       }));
    }
   var logoImageUrl;
   
   var logoImage = Ti.UI.createImageView({
        top:"0dp",
        width: VARS.platformWidth,
        height: ( (VARS.platformHeight / VARS._dpiHeight) > 1.5) ? "187dp" : '249dp' ,
        //height: '187dp',
        image:VARS._platform==VARS._iPad?"/images/logoipad.png":"/images/logo.png",
        preventDefaultImage:true,
   });
   
   if(Ti.Platform.osname != 'android') {
   	if(VARS._platform==VARS._iPad) {
   		logoImage.height = '448dp';	
   	}
   }
   
    var img = Ti.UI.createImageView({
		image: "http://m-kudu.h.edesigner-test.com/sites/default/files/styles/home-image-offer/public/home-image-offer/386.jpg?itok=sWf2w9ml",
		width : '100%',
		height: "100%",
		bottom: 0
	});
	
	//take height from sliderScroll
	var containerImg = Ti.UI.createView({
		//backgroundColor:'#ffccdd',
		bottom: (pagesInfo[0].numRows*itemHeight+10)+"dp",
		//height: '170dp',
		top: logoImage.height,
		width: '100%'
		
	});
	containerImg.add( img );
	
	
	var btnEN = Ti.UI.createButton({
		title: "EN",
		top: '20dp' + logoImage.height
	});
	//self.add( btnEN );
	btnEN.addEventListener('click', function(e) {
		locale.setLocale("en"); 
		sections[0].title = '';	
		Ti.API.info(sections[0]);
		Ti.API.info(sections[0].title);
		
		Titanium.App.fireEvent('changeLanguageEvent');
	});
	var btnAR = Ti.UI.createButton({
		title: "AR",
		top: '50dp'
	});
	btnAR.addEventListener('click', function(e){
		locale.setLocale('ar');
		Titanium.App.fireEvent('changeLanguageEvent');
	}) ;

	
	if(VARS.platformHeight > 500) 
		self.add ( containerImg );
	self.add(logoImage);
	
    self.add(sliderScroll);
    self.containingTab=tabMain;
    tgMetroHolder.addTab(tabMain);
    
    Titanium.App.fireEvent('applicationHomeLOADED');
    
    if(VARS._platform==VARS._android){return self;}
    else {return tgMetroHolder;}

}

function appSliderOpenElem() {
	Titanium.API.info('containing tab: ' + self.containingTab);
	return self.containingTab;
}

module.exports = appSliderOpenElem;
module.exports = ApplicationSlider;
