exports.separatoColor="white";
exports.rowColor="#EBC285";
exports.rowOddColor="#EBC273";
exports.windowBackgroundColor="#FFFFFF";
exports.barColor="#EBC285";
exports.fontColor="#0B0B0B";
exports.imageFrameColor="#EBC285";
exports.formTextColor="#0B0B0B";
exports.formBgColor="#EBC285";
exports.buttonColor="#B37E42";
exports.textColor="#0B0B0B";
exports.textSecondColor="#989898";
exports.listColors=["#fe4819","#d1005d","#0081ab","#009a3d"];

//Optional
exports.videosCoversColor="#F2F2F2";
exports.videosWindowBackgroundColor="#E5E5E5";
exports.videosTextColor="#6D6D6D";
