exports.separatoColor="white";
exports.rowColor="#8c171d";
exports.rowOddColor="#a34940";
exports.windowBackgroundColor="white";
exports.barColor="#8c021c";

exports.textColor="#FFFFFF";
exports.textSecondColor="#FFFFFF";
exports.listColors=["#fe4819","#d1005d","#0081ab","#009a3d"];

exports.fontColor="#ffffff";
exports.imageFrameColor="white";
exports.formTextColor="white";
exports.formBgColor="#8c171d";
exports.buttonColor="#a34940";