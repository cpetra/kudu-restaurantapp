exports.separatoColor="#919194";
exports.rowColor="#98989C";
exports.rowOddColor="#ADADB0";
exports.windowBackgroundColor="#98989C";
exports.barColor="#7C91AE";
exports.textColor="#0B0B0B";
exports.textSecondColor="#4D4D4D";
exports.listColors=["#fe4819","#d1005d","#0081ab","#009a3d"];

//Optional - Video Colors
exports.videosCoversColor="#F2F2F2";
exports.videosWindowBackgroundColor="#E5E5E5";
exports.videosTextColor="#6D6D6D";

//Optional - Contact Colors
exports.fontColor="#0B0B0B";
exports.imageFrameColor="#E5E5E5";
exports.formTextColor="#0B0B0B";
exports.formBgColor="#E5E5E5";
exports.buttonColor="#676767";
